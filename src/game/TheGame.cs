using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Chaotx.Mgx.Views;

namespace GameProject {
    public class TheGame : Game {
        public GraphicsDeviceManager Graphics {get;}
        public GameSettings Settings {get;}

        private SpriteBatch spriteBatch;
        private ViewManager viewManager;

        public TheGame() {
            Content.RootDirectory = "content";
            Graphics = new GraphicsDeviceManager(this);
            viewManager = new ViewManager(Content, Graphics);
            Settings = GameSettings.Load("settings.json");
        }

        protected override void Initialize() {
            base.Initialize();
            Settings.Apply(this);
        }

        protected override void LoadContent() {
            base.LoadContent();
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void Update(GameTime gameTime) {
            base.Update(gameTime);
            viewManager.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            base.Draw(gameTime);
            GraphicsDevice.Clear(Color.Black);
            viewManager.Draw(spriteBatch);
        }
    }
}