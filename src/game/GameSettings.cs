using Microsoft.Xna.Framework;
using Newtonsoft.Json.Linq;

using System.IO;
using System;

namespace GameProject {
    public class GameSettings {
        public Point Resolution {get; protected set;}
        public bool FullScreen {get; protected set;}
        public bool MouseVisible {get; protected set;}
        public float AudioVolume {get; protected set;}
        public float MusicVolume {get; protected set;}

        private GameSettings() {
            // default values
            Resolution = new Point(960, 720);
            MouseVisible = true;
        }

        public static GameSettings Load(string file) {
            var settings = new GameSettings();

            try {
                var obj = JObject.Parse(File.ReadAllText(file));
                settings.Resolution = obj.GetValue("Resolution").ToObject<Point>();
                settings.FullScreen = obj.GetValue("FullScreen").ToObject<bool>();
                settings.MouseVisible = obj.GetValue("MouseVisible").ToObject<bool>();
                settings.AudioVolume = obj.GetValue("AudioVolume").ToObject<float>();
                settings.MusicVolume = obj.GetValue("MusicVolume").ToObject<float>();
            } catch(Exception e) {
                Console.WriteLine(e.Message); // error
                Save(new GameSettings(), file); // save default settings
            }

            return settings;
        }

        public static void Save(GameSettings settings, string file) {
            JObject obj = JObject.FromObject(settings);
            File.WriteAllText(file, obj.ToString());
        }

        public void Apply(TheGame game) {
            game.IsMouseVisible = MouseVisible;
            game.Graphics.HardwareModeSwitch = false;
            game.Graphics.PreferredBackBufferWidth = Resolution.X;
            game.Graphics.PreferredBackBufferHeight = Resolution.Y;
            game.Graphics.IsFullScreen = FullScreen;
            game.Graphics.ApplyChanges();
        }
    }
}