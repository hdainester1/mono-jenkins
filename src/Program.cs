using System;

namespace GameProject {
    public class Program {
        [STAThread]
        public static void Main(string[] args) {
            using(var game = new TheGame())
                game.Run();
        }
    }
}